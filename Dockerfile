FROM golang:latest 
RUN mkdir /app 
ADD . /app/ 
WORKDIR /app 
RUN go get github.com/mmcdole/gofeed
RUN go build -o main . 
CMD ["/app/main"]
EXPOSE 8093
