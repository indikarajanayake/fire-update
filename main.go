package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/mmcdole/gofeed"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fp := gofeed.NewParser()
	feed, _ := fp.ParseURL("http://www.rfs.nsw.gov.au/feeds/majorIncidents.xml")

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.WriteString(w, "<!DOCTYPE html><head><link rel='stylesheet' href='//fonts.googleapis.com/css?family=Lato' /><style>body { width: 960px; max-width: 100%; margin: auto; font-family: 'Lato', sans-serif;}.content { display: inline-block; margin-top: 10px; margin-bottom:10px; border: 1px solid #ccc; border-radius: 5px; width: 50%; padding: 2rem; transition: 0.3s all;} .content:hover{ box-shadow: 0px 5px 0px #ccc} a { text-decoration: none; border: 1px solid #ccc; color: #333; padding: 7px; display: inline-block; transition: 0.5s all; } a:hover { background: #ccc; color: #000}</style><title>"+feed.Title+" - "+feed.Description+"</title><body>")
	fmt.Fprintf(w, "%s", "<h2>"+feed.Title+"</h2>")
	fmt.Fprintf(w, "%s", "<div>")
	for i := 0; i <= len(feed.Items)-1; i++ {
		fmt.Fprintf(w, "%s", "<div>")
		fmt.Fprintf(w, "%s", "<h3>"+feed.Items[i].Title+"</h3>")
		fmt.Fprintf(w, "%s", feed.Items[i].Description)
		fmt.Fprintf(w, "%s", "</div>")
	}
	fmt.Fprintf(w, "%s", "</div>")
	fmt.Fprintf(w, "%s", "</body></html>")
}
func main() {
	http.HandleFunc("/", handler)
	fmt.Println("Starting Restful services...")
	fmt.Println("Using port:8093")
	err := http.ListenAndServe(":8093", nil)
	log.Print(err)
	errorHandler(err)
}
func errorHandler(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
